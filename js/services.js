'use strict';

app.service('langService', [ '$window', function($window){
    this.getLang = function() {
        return ($window.navigator.userLanguage || $window.navigator.language).substring(0,2);
    };
}]);

app.service('resourceService', function($resource){
    
    this.cache = {};
    
    this.getResource = function(path, callback, error) {
        
        var me = this;
        
        if(this.cache[path]) {
            callback(this.cache[path]);
        } else {
           $resource(path).get(function (data) {
               me.cache[path] = data;
               callback(data);
           }, error);
        }
    };
});

app.service('fetchService', ['resourceService', function(resourceService){
    this.fetch = function (path, defaultPath, callback) {
        resourceService.getResource(path, callback, function(){
            resourceService.getResource(defaultPath, callback, function(){
                callback({});
            });
        });
    }; 
}]);

app.service('translationService', ['fetchService', 'langService', function (fetchService, langService) {

    this.getTranslation = function (callback) {
        fetchService.fetch('json/i18n_' + langService.getLang() + '.json', 'json/i18n_fr.json', callback);
    };
}]);

app.service('projectService', ['fetchService', 'langService', function(fetchService, langService){
    
    this.getProjectsList = function(callback) {
        fetchService.fetch('json/projectsList_' + langService.getLang() + '.json', 'json/projectsList_fr.json', callback);
    };
    
    this.getProject = function(project, callback) {
        fetchService.fetch('json/' + project + '_' + langService.getLang() + '.json', 'json/' + project + '_fr.json', callback);
    };
}]);

app.service('aboutService', ['fetchService', 'langService', function(fetchService, langService){
    
    this.getAbout = function(callback) {
        fetchService.fetch('json/about' + '_' + langService.getLang() + '.json', 'json/about' + '_fr.json', callback);
    };
}]);

app.service('resumeService', ['fetchService', 'langService', function(fetchService, langService){
    
    this.getResume = function(callback) {
        fetchService.fetch('json/resume' + '_' + langService.getLang() + '.json', 'json/resume' + '_fr.json', callback);
    };
}]);

app.service('titleService', ['fetchService', 'langService', function(fetchService, langService){

    this.getTitleList = function(callback) {
        fetchService.fetch('json/title' + '_' + langService.getLang() + '.json', 'json/title' + '_fr.json', callback);
    };
    
    this.setTitle = function(title) {
        this.title = title;
    };
    
    this.getTitle = function() {
        return this.title;
    };
}]);
