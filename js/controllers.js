﻿'use strict';

// optional controllers
app.controller('IndexCtrl', ['$rootScope', '$scope', 'translationService', 'projectService', 'titleService', function($rootScope, $scope, translationService, projectService, titleService){

    $rootScope.title = "Projects | Stephane Baudoux | Personal Website";
    titleService.getTitleList(function(data) {
        $rootScope.title = data.index + data.main;
    });
    
    translationService.getTranslation(function(data) {
        $scope.translation = data; 
    });

    projectService.getProjectsList(function(data) {
        $scope.projectsList = data; 
    });
    
    $scope.getProjectBackgroundStyle = function(project) {
        return project ? {
            'background-color': project.background
        } : "";
    };
    
    $scope.goToContact = function() {
        window.location.href = 'mailto:contact@stephane-baudoux.com';
    };
}]);

// optional controllers
app.controller('HomeCtrl', ['$rootScope', 'titleService', function($rootScope, titleService){
        
    $rootScope.title = "Projects | Stephane Baudoux | Personal Website";
    titleService.getTitleList(function(data) {
        $rootScope.title = data.index + data.main;
    });
}]);

app.controller('ProjectCtrl', ['$rootScope', '$scope', 'projectService', 'projectId', 'titleService', function($rootScope, $scope, projectService, projectId, titleService){
    
    $scope.slideShowCounter = 0;
    
    $rootScope.title = "Projects | Stephane Baudoux | Personal Website";
    titleService.getTitleList(function(data) {
        $rootScope.title = data[projectId] + data.main;
    });

    projectService.getProject(projectId, function(data) {
        $scope.project = data;
    });
    
    $scope.getProjectBackgroundStyle = function() {
        return $scope.project ? {
            'background-color': $scope.project.background
        } : "";
    };
    
    $scope.getProjectLinkBackgroundStyle = function() {
        return $scope.project ? {
            'color': $scope.project.background
        } : "";
    };
    
    // Called when clicking on a project image
    $scope.showSlideShow = function() {
        $scope.enableSlideShow = true;
        $scope.slideShowIndex = 0;
    };
    
    // Called when clicking outside the slideshow
    $scope.hideSlideShow = function($event) {
        if($($event.target).hasClass('slideshowContainer')) {
            $scope.enableSlideShow = false;
        } 
    };
    
    $scope.getStyle = function(slide) {
        return slide === $scope.project.images.slideshow[0] ? $scope.slideshowStyle : '{}';
    };
    
    $scope.previousSlideShow = function(e) {
        
        if($scope.slideShowCounter > 0) {
        
            $scope.slideShowCounter--;
        
            $scope.slideshowStyle = {
                'transition': '1s', 
                'margin-left': -$scope.slideShowCounter * 100 + '%'
            };

        }
        
        e.preventDefault();
    };
    
    $scope.nextSlideShow = function(e) {
        
        if($scope.slideShowCounter + 1 < $scope.project.images.slideshow.length) {
        
            $scope.slideShowCounter++;
        
            $scope.slideshowStyle = {
                'transition': '1s', 
                'margin-left': -$scope.slideShowCounter * 100 + '%'
            };
        }
        
        e.preventDefault();
    };
}]);

app.controller('ResumeCtrl', ['$rootScope', '$scope', 'resumeService', 'titleService', function($rootScope, $scope, resumeService, titleService){
    $rootScope.title = "Resume | Stephane Baudoux | Personal Website";
    titleService.getTitleList(function(data) {
        $rootScope.title = data.resume + data.main;
    });
    
    resumeService.getResume(function(data) {
        $scope.resume = data;
    });
}]);

app.controller('AboutCtrl', ['$rootScope', '$scope', 'aboutService', 'titleService', function($rootScope, $scope, aboutService, titleService){
    $rootScope.title = "About | Stephane Baudoux | Personal Website";
    titleService.getTitleList(function(data) {
        $rootScope.title = data.about + data.main;
    });
    
    aboutService.getAbout(function(data) {
        $scope.about = data;
    });
}]);
