function slideLeft (width){       
    $(".slideshow ul li:first-child").animate({"margin-left": -width}, 800, function(){    
        $(this).css("margin-left",0).appendTo(".slideshow ul");    
    });       
};

function slideRight (width){       
    $(".slideshow ul li:first-child").animate({"margin-left": width}, 800, function(){    
        $(this).css("margin-left",0).appendTo(".slideshow ul");    
    });       
};  
