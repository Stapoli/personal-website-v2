﻿'use strict';

// App initialization
var app = angular.module('PWV2', ['ngResource', 'ngRoute']).
    config(['$routeProvider', function ($routeProvider) {
      $routeProvider.
        when('/', { 
            templateUrl: 'pages/index.html', 
            activetab: 'projects', 
            controller: 'HomeCtrl'
        }).
        when('/project/:projectId', {
          templateUrl: function () { 
              return 'pages/project.html'; 
          },
          controller: 'ProjectCtrl',
          activetab: 'projects',
          resolve: {
            projectId: function($route){
                return $route.current.params.projectId;
            }
          }
        }).
        when('/resume', {
          templateUrl: 'pages/resume.html',
          controller: 'ResumeCtrl',
          activetab: 'resume'
        }).
        when('/about', {
          templateUrl: 'pages/about.html',
          controller: 'AboutCtrl',
          activetab: 'about'
        }).
        otherwise({ redirectTo: '/' });
    }]).run(['$rootScope', '$http', '$route', function ($scope, $http, $route) {

        $scope.$on("$routeChangeSuccess", function (/*scope, next, current*/) {
          $scope.part = $route.current.activetab;
        });
  }]);

app.config(['$locationProvider', function($location) {
    $location.hashPrefix('!');
}]);

